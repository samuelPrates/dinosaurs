import csv


def import_from_file(url):
    with open(url) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        lst = []
        for row in csv_reader:
            lst.append(row)
        return lst
