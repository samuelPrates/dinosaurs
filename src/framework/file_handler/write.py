import csv


def write_into_file(url, list):
    try:
        with open(url, 'wt') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for i in list:
                spamwriter.writerow(i)
        return True
    except Exception as e:
        return False
