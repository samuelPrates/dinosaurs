from src.domain.entity.dinosaur import Dinosaur


class Stance_Filter:
    def filter(self, params, stance):
        dino = Dinosaur()
        return [i for i in params if type(i) == type(dino) and i.STANCE == stance]
