from src.domain.filter.stance_filter import Stance_Filter


def bipedal_filter(params):
    stc = Stance_Filter()
    return stc.filter(params, 'bipedal')
