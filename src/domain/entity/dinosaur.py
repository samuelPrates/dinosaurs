class Dinosaur:
    NAME = ""
    LEG_LENGTH = 0
    DIET = ""
    STRIDE_LENGTH = 0
    STANCE = ""
    _g = 9.8

    def __init__(self, *args):
        for i in args:
            self.update_args(i)

    def update_args(self, dict_itens):
        for i, y in dict_itens.items():
            if i == "NAME" and self.NAME in ("", None):
                self.NAME = y
            if i == "LEG_LENGTH" and self.LEG_LENGTH in (0, None):
                self.LEG_LENGTH = float(y)
            if i == "DIET" and self.DIET in ("", None):
                self.DIET = y
            if i == "STRIDE_LENGTH" and self.STRIDE_LENGTH in (0, None):
                self.STRIDE_LENGTH = float(y)
            if i == "STANCE" and self.STANCE in ("", None):
                self.STANCE = y

    def merge(self, dino):
        if type(dino) == type(self):
            if dino.NAME not in ("", None) and self.NAME in ("", None):
                self.NAME = dino.NAME
            if dino.LEG_LENGTH not in (0, None) and self.LEG_LENGTH in (0, None):
                self.LEG_LENGTH = float(dino.LEG_LENGTH)
            if dino.DIET not in ("", None) and self.DIET in ("", None):
                self.DIET = dino.DIET
            if dino.STRIDE_LENGTH not in (0, None) and self.STRIDE_LENGTH in (0, None):
                self.STRIDE_LENGTH = float(dino.STRIDE_LENGTH)
            if dino.STANCE not in ("", None) and self.STANCE in ("", None):
                self.STANCE = dino.STANCE

    def get_velocity(self):
        if self.STRIDE_LENGTH in ("", None) or self.LEG_LENGTH in ("", None):
            return False
        else:
            return ((self.STRIDE_LENGTH / self.LEG_LENGTH) - 1 * ((self.LEG_LENGTH * self._g) ** (1 / 2)))

    def get_names(self):
        return ["NAME", "LEG_LENGTH", "DIET", "STRIDE_LENGTH", "STANCE"]

    def get_list(self):
        return [str(self.NAME), str(self.LEG_LENGTH), str(self.DIET), str(self.STRIDE_LENGTH), str(self.STANCE)]
