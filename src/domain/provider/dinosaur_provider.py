from src.domain.entity.dinosaur import Dinosaur


class DinosaurProvider:
    def get_dinosaur_from_list(self, list=[{}]):
        lst = []
        for i in list:
            dino = Dinosaur(i)
            lst.append(dino)
        return lst

    def get_dinosaur_from_lists(self, list=[{}], list2=[{}]):
        lst = self.get_dinosaur_from_list(list)
        lst2 = self.get_dinosaur_from_list(list2)
        for i in lst:
            for y in lst2:
                if i.NAME == y.NAME:
                    i.merge(y)

        return lst
