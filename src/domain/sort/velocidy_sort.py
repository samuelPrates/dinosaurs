class VelocitySort:
    def asc(self, list):
        return sorted(list, key=lambda i: i.get_velocity())

    def desc(self, list):
        return sorted(list, key=lambda i: i.get_velocity(), reverse=True)
