from src.framework.file_handler.write import write_into_file


class WriterIntoFile:
    def input(self, url, list):
        new_list = [list[0].get_names()]
        [new_list.append(i.get_list()) for i in list]
        return write_into_file(url, new_list)
