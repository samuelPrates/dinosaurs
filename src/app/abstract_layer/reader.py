from src.framework.file_handler.extract import import_from_file


class ReadFromFile:
    def get_dict(self, url):
        list = import_from_file(url)
        first = list.pop(0)
        dict_list = [dict(zip(first, i)) for i in list]
        return dict_list
