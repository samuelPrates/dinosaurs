from src.app.abstract_layer.reader import ReadFromFile
from src.domain.provider.dinosaur_provider import DinosaurProvider
from src.domain.filter.bipedal_filter import bipedal_filter
from src.domain.sort.velocidy_sort import VelocitySort
from src.app.abstract_layer.writer import WriterIntoFile


class App:
    def run(self, dir="./../Documents/"):
        reader = ReadFromFile()
        writer = WriterIntoFile()
        data_provider = DinosaurProvider()
        sort = VelocitySort()
        file1 = dir + "dataset1.csv"
        file2 = dir + "dataset2.csv"
        output_file = dir + "output.txt"
        list1 = reader.get_dict(file1)
        list2 = reader.get_dict(file2)
        dinosaurs = data_provider.get_dinosaur_from_lists(list1, list2)
        dinosaurs_filtered = bipedal_filter(dinosaurs)
        dinosaurs_sorted = sort.desc(dinosaurs_filtered)
        writer.input(output_file, dinosaurs_sorted)
        return dinosaurs_sorted
