import unittest
from tests.mock_provider import DinossaurList
from src.domain.provider.dinosaur_provider import DinosaurProvider
from src.app.abstract_layer.reader import ReadFromFile


class TestDinosaurProvider(unittest.TestCase):
    def test_get_dinosaur_from_list(self):
        dino_list = DinossaurList()
        reader = ReadFromFile()
        list = reader.get_dict('./documents/dataset3.csv')
        provider = DinosaurProvider()
        result = provider.get_dinosaur_from_list(list=list)
        list_4_and_5 = dino_list.get_list_4_and_5()
        self.assertEqual(result[0].NAME, list_4_and_5[0].NAME)
        self.assertEqual(result[1].DIET, list_4_and_5[1].DIET)


if __name__ == '__main__':
    unittest.main()
