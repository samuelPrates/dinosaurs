import unittest
from src.domain.filter.bipedal_filter import bipedal_filter
from tests.mock_provider import DinossaurList


class TestFiltersByBipedal(unittest.TestCase):
    def test_filter(self):
        dino_list = DinossaurList()
        list_filtered = bipedal_filter(dino_list.get_list())
        result = [dino_list.get_dino_3()]
        self.assertEqual(list_filtered, result)


if __name__ == '__main__':
    unittest.main()
