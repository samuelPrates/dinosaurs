import unittest
from src.framework.file_handler.extract import import_from_file


class TestImportResults(unittest.TestCase):
    def test_differ_files(self):
        result_a = import_from_file('./documents/dataset1.csv')
        result_b = import_from_file('./documents/dataset2.csv')
        self.assertNotEqual(result_a, result_b)
        self.assertEqual(result_a.__len__(), result_b.__len__())


if __name__ == '__main__':
    unittest.main()
