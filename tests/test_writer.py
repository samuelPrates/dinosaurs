import unittest
from src.app.abstract_layer.writer import WriterIntoFile
from tests.mock_provider import DinossaurList


class TestWriter(unittest.TestCase):
    def test_writer(self):
        dino_list = DinossaurList()
        writer = WriterIntoFile()
        list = dino_list.get_list()
        resp = writer.input('./documents/output.txt', list)
        self.assertEqual(True, resp)


if __name__ == '__main__':
    unittest.main()
