import unittest
from src.app.abstract_layer.reader import ReadFromFile


class TestReaderAbstraction(unittest.TestCase):
    def test_dict_object(self):
        reader = ReadFromFile()
        result = reader.get_dict('./documents/dataset3.csv')
        dict_lst = [
            {
                "NAME": "Hadrosaurus",
                "LEG_LENGTH": "1.2",
                "DIET": "herbivore"
            },
            {
                "NAME": "Struthiomimus",
                "LEG_LENGTH": "0.92",
                "DIET": "omnivore"
            }
        ]
        self.assertEqual(result, dict_lst)


if __name__ == '__main__':
    unittest.main()
