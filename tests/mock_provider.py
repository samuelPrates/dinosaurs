from src.domain.entity.dinosaur import Dinosaur


class DinossaurList:
    def __init__(self):
        self.dino = Dinosaur({"NAME": 'Hadrosaurus', "LEG_LENGTH": "1.2", "DIET": 'herbivore', "STRIDE_LENGTH": "7",
                              "STANCE": 'stance_test'})
        self.dino2 = Dinosaur({"NAME": 'Struthiomimus', "LEG_LENGTH": "0.92", "DIET": 'omnivore', "STRIDE_LENGTH": "8",
                               "STANCE": 'stance2_test'})
        self.dino3 = Dinosaur({"NAME": 'name3_test', "LEG_LENGTH": "6", "DIET": 'diet3_test', "STRIDE_LENGTH": "12",
                               "STANCE": 'bipedal'})
        self.dino4 = Dinosaur({"NAME": 'Hadrosaurus', "LEG_LENGTH": "1.2", "DIET": 'herbivore'})
        self.dino5 = Dinosaur({"NAME": 'Struthiomimus', "LEG_LENGTH": "0.92", "DIET": 'omnivore'})

    def get_list(self):
        return [self.dino, self.dino2, self.dino3]

    def get_dino_1(self):
        return self.dino

    def get_dino_2(self):
        return self.dino2

    def get_dino_3(self):
        return self.dino3

    def get_list_4_and_5(self):
        return [self.dino4, self.dino5]
