import unittest
from src.app.run_app import App

class TestAppWorking(unittest.TestCase):
    def test_app_run(self):
        app = App()
        list = app.run('./documents/')
        self.assertEqual(list.__len__(), 4)


if __name__ == '__main__':
    unittest.main()
