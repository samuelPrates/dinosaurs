import unittest
from tests.mock_provider import DinossaurList
from src.domain.sort.velocidy_sort import VelocitySort


class TestVelocidySort(unittest.TestCase):
    def test_velocidy_sort_asc(self):
        dino_list = DinossaurList()
        sort_velocity = VelocitySort()
        list_initial = dino_list.get_list()
        list_sorted = sort_velocity.asc(list_initial)
        self.assertNotEqual(list_initial, list_sorted)
        self.assertGreater(list_initial[0].get_velocity(), list_sorted[0].get_velocity())
        self.assertEqual(list_initial.__len__(), list_sorted.__len__())

    def test_velocidy_sort_desc(self):
        dino_list = DinossaurList()
        sort_velocity = VelocitySort()
        list_initial = dino_list.get_list()
        list_sorted = sort_velocity.desc(list_initial)
        self.assertNotEqual(list_initial, list_sorted)
        self.assertGreater(list_sorted[0].get_velocity(), list_initial[0].get_velocity())
        self.assertEqual(list_initial.__len__(), list_sorted.__len__())


if __name__ == '__main__':
    unittest.main()
